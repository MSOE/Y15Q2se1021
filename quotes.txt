I probably won't assign many homeworks, but the ones I do assign will be long and time consuming.--1-9-96
The quote.
I don't want to embarrass you. You can raise your hand lightly, but who doesn't know what a maximum likelihood estimator is?--1-9-96
What I am about to say is important...this is where you want to pay attention.--1-9-96
If there was a die hard frequentest in the room, I would probably be assassinated on the spot.--1-9-96
If you have no data, you have a problem situation.--1-9-96
You do a little soul searching and ask yourself, "What is unsatisfying about this solution?"--1-9-96
RV means random variable, not recreational vehicle.--1-11-96
It's okay to use abusive notation as long as it is clear in your mind that you are being very abusive.--1-11-96
Sometimes this point seems perfectly clear even when it isn't.--1-11-96
I may as well apologize for being a little sloppy in what I am going to do next.--1-11-96
Oh goodness, I'm late and I'm the first one here. [a bit of an exaggeration]--1-16-96
I was late for class today, but I think I'm going to have to put my foot down and demand that I show up on time and demand that you do too.--1-16-96
I'm going to write this in a non-causal way. [Writes notation and then the equation]--1-16-96
The nice thing is that if my formula were wrong, it wouldn't really matter...I'd still get the right answer.--1-16-96
Make this a little aside. I'm going off on a tangent.--1-16-96
