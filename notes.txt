. A subclass inherits attributes and behavior from a superclass using the keyword: *extends*
. A subclass can only have one superclass
. The subclass can access public attributes and behaviors
. The subclass can NOT access private attributes and behaviors
. The subclass can access protected attributes and behaviors
. The reference type of a variable determines what methods/attributes can be
  accessed through the reference variable
. A reference type can refer to objects of the same type or any subtypes
. Type X is-a Y implies inheritance, e.g., a TextFile is-a File (TextFile extends File)
. Type X has-a Y implies composition, e.g., a Circle has-a Color
. An interface advertises a particular interface but does not provide an implementation
. An interface cannot be instantiated
. References may have a class or interface type
. A method identified as abstract does not have an implementation provided
. A class with at least one abstract method must be declared as abstract
. An abstract class cannot be instantiated
. Subclasses of an abstract class must be abstract unless they implement all of the
  superclass's abstract methods

