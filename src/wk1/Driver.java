package wk1;

/* OUTCOMES - Java Fundamentals
 +  Be aware of the memory requirements and value ranges for primitive types
    Type   Size (bits)
    ====== ======
    byte   8
    short  16
    int    32
    long   64
    float  32
    double 64
    char   16
 +  Interpret code in which automatic type conversions are present
    Narrower                                   Wider
     byte -> short -> int -> long -> float -> double
              char ->
 +  Use type casting to explicitly convert data types
 +  Explain the risks associated with explicit type casting
 +  Use mathematical operations to manipulate characters
 +  Use increment and decrement operators
 +  Explain how pre- and post- increment/decrement operators differ in functionality
 -  Use short-circuit evaluation to avoid divide-by-zero and null-pointer exceptions
 */

import javax.swing.*;

public class Driver {
    public static void main(String[] args) {
        A aaa = new A();
        aaa.add("first");
        A bbb = aaa;
        A ccc = new A();
        System.out.println(aaa.equals("string"));
        System.out.println(aaa + "\n" + ccc.toString());
    }

    public static void main4(String[] args) {
        String input = JOptionPane.showInputDialog("Enter something");
        if(input!=null && input.length()>=3) {
            System.out.println(input + input);
        }
    }


    public static void main3(String[] args) {
        int i = 3;
        int j = i++ + --i; // Don't write code like this
        System.out.println(j);
        System.out.println(i);
    }

    public static void main2(String[] args) {
        String word = "The cows moo in the morning.";
        String jbeq = "";
        for(int i=0; i<word.length(); ++i) {
            jbeq += rot13(rot13(word.charAt(i)));
        }
        System.out.println(jbeq);
    }

    //abcdefghijklm nopqrstuvwxyz
    public static char rot13(char letter) {
        char answer = letter;
        if(letter>='a' && letter<='z') {
            int position = letter - 'a';
            answer = (char)('a' + (position+13)%26);
        }
        return answer;
    }
}




















