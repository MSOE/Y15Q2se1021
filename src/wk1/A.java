package wk1;

/*
    Use inheritance in creating classes
    Explain why a class with a superclass other than object should make an explicit call to a constructor of the superclass if no default constructor is present in the superclass
    Define aggregation
    Define composition
    Use aggregation and composition within user defined classes
    Explain what is meant by "overriding a method"
    Make use of super reference to call a parent method from within a method that overrides it
    Read and understand UML class and sequence diagrams
    Implement source that meets the design specified in a UML class and sequence diagram
 */

import java.util.ArrayList;

public class A extends ArrayList<String> {

    @Override
    public boolean equals(Object that) {
        return true;
//        return (that instanceof A);
//        boolean same = false;
//        if(that instanceof A) {
//            same = true;
//        }
//        return same;
    }

    @Override
    public String toString() {
        return "This is silly";
    }
}
