package wk4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DogPony extends JFrame implements ActionListener {

    public static void main(String[] args) {
        new DogPony("Dog and Pony Show");
    }

    private JLabel animalLabel;
    private JLabel countLabel;
    private int count = 0;

    public DogPony(String title) {
        super(title);
        setSize(250, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());
        animalLabel = new JLabel("Dog");
        add(animalLabel);
        JButton button = new JButton("Toggle Animal");
        button.addActionListener(this);
        add(button);
        JButton button2 = new JButton("Concat Dog");
        button2.addActionListener(this);
        add(button2);
        countLabel = new JLabel("" + count);
        add(countLabel);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Toggle Animal")) {
            ++count;
            countLabel.setText("" + count);
            if (animalLabel.getText().charAt(0) == 'D') {
                animalLabel.setText("Pony");
            } else {
                animalLabel.setText("Dog");
            }
        } else {
            animalLabel.setText(animalLabel.getText() + " Dog");
        }
    }
}
