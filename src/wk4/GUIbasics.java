package wk4;

import javax.swing.*;
import java.awt.*;

public class GUIbasics {
    public static void main(String[] args) {
//        jop1();
//        jop2();
//        jop3();
//        jop4();
        jframe();
    }


    private static void jop1() {
        JOptionPane.showMessageDialog(null, "Hello.");
    }

    private static void jop2() {
        JOptionPane.showInputDialog(null, "Enter a goofy number",
                "I am a title, see me roar", JOptionPane.ERROR_MESSAGE);
    }

    private static void jop3() {
        if(JOptionPane.NO_OPTION == JOptionPane.showConfirmDialog(null, "Play another game?", "Confirmation",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)) {
            System.out.println("Done");
        } else {
            System.out.println("Continuing");
        }
    }

    private static void jop4() {
        Object[] options = { "Bye", "Second", "Eric", "Fourth", "Cancel (just kidding)" };
        JOptionPane.showOptionDialog(null, "Game over; you're broke.", "Title",
                JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
                null, options, options[1]);
    }

    private static void jframe() {
        JFrame frame = new DogPony("Dog and Pony Show");
        frame.setSize(250, 100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());
        frame.add(new JLabel("Dog"));
        JButton button = new JButton("Print Pony");
        frame.add(button);
        frame.setVisible(true);
    }
}
