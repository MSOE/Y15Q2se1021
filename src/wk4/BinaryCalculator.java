package wk4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class BinaryCalculator extends JFrame implements WindowListener {

    private static final int WIDTH = 170;
    private static final int HEIGHT = 300;
    private JTextField display;
    private String addend1 = "";

    public static void main(String[] ignored) {
        new BinaryCalculator();
    }

    public BinaryCalculator() {
        super("Calculator");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocation(1100, 50);
        setLayout(new BorderLayout());
        Container pane = getContentPane();
        display = new JTextField(12);
        display.setEditable(false);
        pane.add(display, BorderLayout.PAGE_START);
        pane.add(createButtonPanel(), BorderLayout.CENTER);
        JButton clear = new JButton("Clear");
        pane.add(clear, BorderLayout.PAGE_END);
        clear.addActionListener(e -> {
            display.setForeground(Color.BLACK);
            display.setText("");
        });
        addWindowListener(this);

        setVisible(true);
    }

    private JPanel createButtonPanel() {
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(2, 2, 8, 8));
        addNumbers(buttonsPanel);
        JButton plus = new JButton("+");
        JButton equals = new JButton("=");
        buttonsPanel.add(plus);
        buttonsPanel.add(equals);
        plus.addActionListener(e -> {
            addend1 = display.getText();
            display.setText("");
        });
        equals.addActionListener(e -> { handleEquals(e); });
        return buttonsPanel;
    }

    private void addNumbers(JPanel panel) {
        for(char digit='0'; digit<='1'; ++digit) {
            JButton number = new JButton("" + digit);
            number.addActionListener(e -> {
                display.setText(display.getText() + e.getActionCommand());
            });
            panel.add(number);
        }
    }

    // Begin WindowListener methods
    @Override
    public void windowOpened(WindowEvent e) {
        System.out.println("Window opened");
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.err.println("Window closing");
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.err.println("Window closed");
    }

    @Override
    public void windowIconified(WindowEvent e) {
        System.out.println("Window iconified");
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        // Do nothing
    }

    @Override
    public void windowActivated(WindowEvent e) {
        // Do nothing
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        // Do nothing
    }
    // End WindowListener methods

    private void handleEquals(ActionEvent e) {
        String addend2 = display.getText();
        if(addend1.equals("") || addend2.equals("")) {
            display.setText("ERROR");
            display.setForeground(Color.RED);
        } else {
            display.setForeground(Color.BLACK);
            int result = Integer.parseInt(addend1, 2) +
                    Integer.parseInt(addend2, 2);
            addend1 = Integer.toBinaryString(result);
            display.setText(addend1);
        }
    }
}
