package wk3;

import java.time.LocalDateTime;

public abstract class File {
    private String name;
    private LocalDateTime timestamp;

    public File(String name) {
        this.name = name;
        updateTimestamp();
    }

    public String getName() {
        return name;
    }

    public abstract int getSize();

    public void rename(String name) {
        this.name = name;
        updateTimestamp();
    }

    protected void updateTimestamp() {
        timestamp = LocalDateTime.now();
    }
}
