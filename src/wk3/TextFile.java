package wk3;

public class TextFile extends File {
    private String content;

    public TextFile(String name) {
        this(name, null);
    }

    public TextFile(String name, String content) {
        super(name);
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
        updateTimestamp();
    }

    @Override
    public int getSize() {
        return content.length()*2;
    }
}
