package wk3;

import java.util.List;

abstract public class FunFile extends File {
    FunFile() {
        super("fun");
    }
}
