package wk3;

import java.util.ArrayList;
import java.util.List;

public class Folder extends File implements Group {
    private List<File> files;

    public Folder(String name) {
        super(name);
        files = new ArrayList<>();
    }

    public boolean add(File file) {
        boolean uniqueName = true;
        int i = 0;
        while(uniqueName && i<files.size()) {
            if(file.getName().equals(files.get(i).getName())) {
                uniqueName = false;
            }
            ++i;
        }
        if(uniqueName) {
            files.add(file);
            updateTimestamp();
        }
        return uniqueName;
    }

    public File remove(String name) {
        File removed = null;
        for(int i=0; removed!=null && i<files.size(); ++i) {
            if(name.equals(files.get(i).getName())) {
                removed = files.remove(i);
                updateTimestamp();
            }
        }
        return removed;
    }

    @Override
    public int getSize() {
        int sum = 0;                            // for(int i=0; i<files.size(); ++i) {
        for(File file : files) {                //     File file = files.get(i);
            sum += file.getSize();              //     sum += file.getSize();
        }                                       // }
        return sum;
    }
}









