package wk6;

import labs.lab4.FarmAnimal;
import labs.lab4.Gold;
import labs.lab4.Horse;
import labs.lab4.MarketPrice;

import javax.swing.*;

public class Driver {
    public static void main(String[] args) {
        finallyExample("ignored");
//        int i = askUserForInteger("Enter an integer, please");
//        JOptionPane.showMessageDialog(null, "You entered one less than " + (i + 1));
    }

    public static int finallyExample(String prompt) {
        try {
            System.out.println("1");
            if (Math.random() < 0.5) {
                int i = 0 / 0;
            }
            System.out.println("2");
//        } catch(Exception e) {
//            System.out.println(e.getMessage());
        } finally {
            System.out.println("Hi mom");
        }
        return 0;
    }

    public static int askUserForInteger(String prompt) {
        String input = null;
        int result = 0;
        while(input==null) {
            input = JOptionPane.showInputDialog(null, prompt);
            try {
                result = Integer.parseInt(input);
            } catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "You must enter an integer");
                input = null;
                throw new RuntimeException("This is just stupid");
            }
        }
        return result;
    }

    public static void generateException2() {
        generateException();
    }
    public static void generateException() {
        int x = 1/0;
    }
}
