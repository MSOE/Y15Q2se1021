package wk8;

import com.sun.org.apache.xpath.internal.SourceTree;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;

public class Driver {
    public static void main(String[] args) {
        String filename = null;
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "JPG & GIF Images", "jpg", "gif");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            filename = chooser.getSelectedFile().getName();
        }
        try {
            writeObject(filename);
            readObject(filename);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void readObject(String filename) throws ClassNotFoundException {
        try (FileInputStream fin = new FileInputStream(filename);
             ObjectInputStream din = new ObjectInputStream(fin)) {
            System.out.println(din.readDouble());
            Object x = din.readObject();
            System.out.println(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeObject(String filename) {
        try (FileOutputStream fin = new FileOutputStream(filename);
             ObjectOutputStream dout = new ObjectOutputStream(fin)) {
            dout.writeDouble(Math.PI);
            dout.writeObject(new CustomClass());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void highLevelRead(String filename) {
        try (FileInputStream fin = new FileInputStream(filename);
            DataInputStream din = new DataInputStream(fin)) {
            System.out.println(din.readDouble());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeText(String filename) {
        try (PrintWriter out = new PrintWriter(filename)) {
            out.print("stuff");
            out.println("more stuff");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main2(String[] args) {
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream("data.dat");
            fout.write(0);
            fout.write(65);
            fout.write(65);
            fout.write(97);
            fout.write(97);
            fout.write(97);
            fout.write(12);
            fout.write(12);
            fout.write(12);
        } catch (FileNotFoundException e) {
            System.out.println("Could not find the file");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                    System.out.println("Error closing file.");
                }
            }
        }
    }
}
