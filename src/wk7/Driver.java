package wk7;

import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.NoSuchFileException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/*
 * Questions:
  * What happens if file does not exist?
  * What happens if file is empty?
  * What happens if line does not follow the QUOTE--DATE format? E.g.,
   * The quote.
   * The quote -- include dashes--1-22-2015
   * BLANK
   * --1-22-2015
  * Did you find any errors in the program?
 */
public class Driver {

    private static final Logger LOGGER = Logger.getLogger(Driver.class.getName());
    private static final String LOGFILE = "runtime.log";

    public static void main(String[] args) {
        Driver lecture = new Driver();

        try {
            lecture.setUpLogger();
        } catch (IOException e) {
            throw new RuntimeException("The logfile could not be created.");
        }

        Scanner in = new Scanner(System.in);
        QuoteParser parser = new QuoteParser();
        String userInput = "quotes.txt";

//        System.out.println("Enter a filename ('q' to quit):");
//        userInput = in.nextLine();
        LOGGER.info("User entered '" + userInput + "' for the filename.");

        try {
            List<Quote> quotes = parser.parse(userInput, LOGGER);

            quotes.forEach(quote -> {
                LOGGER.info("Quote date: " + quote.getDate());
                System.out.println(quote);
            });

        } catch (InvalidPathException e) {
            LOGGER.warning("Invalid path.");
            System.out.println("The path to the quote file was invalid.");
        } catch (NoSuchFileException e) {
            LOGGER.warning("File does not exist.");
            System.out.println("The quote file does not exist.");
        } catch (IllegalArgumentException e) {
            LOGGER.warning("Quote file is improperly formatted.");
            System.out.println("The quote file contained an improperly formatted line.");
        } finally {
            LOGGER.info("Program done.");
        }
    }

    public void setUpLogger() throws IOException {
        FileHandler logfileHandler;
        SimpleFormatter logfileFormatter;

        logfileHandler = new FileHandler(LOGFILE);
        logfileFormatter = new SimpleFormatter();
        logfileHandler.setFormatter(logfileFormatter);

        LOGGER.setUseParentHandlers(false);
        LOGGER.addHandler(logfileHandler);
    }
}
