package wk7;

public class Quote {
    private String quote;
    private String date;

    public Quote(String quote, String date) {
        this.quote = quote;
        this.date = date;
    }

    public String toString() {
        return quote + " WAS SAID ON " + date + "\n";
    }

    public String getDate() {
        return date;
    }

}

