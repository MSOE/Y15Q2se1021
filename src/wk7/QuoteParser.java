package wk7;

import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class QuoteParser {
    private static final String DELIMITER = "--";

    public List<Quote> parse(String filename, Logger LOGGER) throws InvalidPathException, NoSuchFileException {
        List<Quote> quotes = new ArrayList<>();
        List<String> lines = parseFile(filename, LOGGER);

        lines.forEach(line -> {
            quotes.add(parseQuote(line, LOGGER));
        });

        return quotes;
    }

    private  List<String> parseFile(String filename, Logger LOGGER) throws InvalidPathException, NoSuchFileException {
        List<String> quotes = new ArrayList<>();
        String currentLine = "";
        Scanner fileIn;
        try {
            Path path = Paths.get(filename);
            fileIn = new Scanner(path);

            while (fileIn.hasNextLine()) {
                currentLine = fileIn.nextLine();
                if (!currentLine.isEmpty()) {
                    quotes.add(currentLine);
                }
            }
        } catch (InvalidPathException | NoSuchFileException e) {
            LOGGER.severe(e.getClass() + "\n" + e.getMessage());
            throw e;
        } catch (IOException e) {
            LOGGER.severe(e.getClass() + "\n" + e.getMessage());
            throw new RuntimeException("I/O error reading parse file.");
        }
        return quotes;
    }

    private Quote parseQuote(String line, Logger LOGGER) throws IllegalArgumentException {
        String parts[];
        if (line.contains(DELIMITER)) {
            parts = line.split(DELIMITER);
            if (parts.length != 2) {
                LOGGER.severe("Error parsing line: '" + line + "'.  Does not contain two fields.");
                throw new IllegalArgumentException("Improperly formatted line in parse file.");
            }
        } else {
            LOGGER.severe("Error parsing line: '" + line + "'.  No delimiter found.");
            throw new IllegalArgumentException("Line does not contain delimiter.");
        }
        return new Quote(parts[0], parts[1]);
    }
}
