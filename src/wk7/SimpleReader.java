package wk7;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Simple text file reader example.
 */
public class SimpleReader {
    public static void main(String[] args) {
        String filename = "notes.txt";
        Path path = Paths.get(filename);
        try (Scanner fin = new Scanner(path)) {
            while(fin.hasNext()) {
                System.out.println(fin.next());
            }
        } catch (IOException e) {
            System.out.println("Encountered an error, and gave up.  So sorry.");
        }
    }
}
