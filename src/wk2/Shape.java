package wk2;

import java.awt.Color;

public class Shape {
    private Color color;
    private double xCoord;
    private double yCoord;

    public Shape() {
        color = Color.WHITE;
        yCoord = 0.0;
        System.out.println("Shape: constructor");
    }

    public void draw() {
        System.out.println("Shape: draw");
    }

    public void erase() {
        System.out.println("Shape: erase");
    }

    public void move() {
        xCoord += 5.0;
        yCoord += 5.0;
        System.out.println("Shape: move");
    }

    public void zoom(double magnification) {
        System.out.println("Shape: zoom");
    }

    public Color getColor() {
        System.out.println("Shape: getColor");
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        System.out.println("Shape: setColor");
    }
}
