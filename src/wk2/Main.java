package wk2;

/*
. A subclass inherits attributes and behavior from a superclass using the keyword: extends
. The subclass can access public attributes and behaviors
. The subclass can NOT access
 */
public class Main {
    public static void main(String[] args) {
        Shape shape = new Shape();
        Circle circle = new Circle();
        shape.move();
        circle.move();
        shape.draw();
        circle.draw();
        shape.zoom(0.5);
        circle.zoom(0.5);
        shape.erase();
        circle.erase();
        circle.getRadius();
    }
}
