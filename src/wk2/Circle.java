package wk2;

public class Circle extends Shape {
    private double radius;

    public Circle() {
        radius = 0.0;
        System.out.println("Circle: constructor");
    }

    @Override
    public void draw() {
        System.out.println("Circle: draw");
    }

    public void erase() {
        super.erase();
        System.out.println("Circle: erase");
    }

    public void zoom(double magnification) {
        radius *= magnification;
        System.out.println("Circle: zoom");
    }

    public void setRadius(double radius) {
        this.radius = radius;
        System.out.println("Circle: setRadius");
    }

    public double getRadius() {
        System.out.println("Circle: getRadius");
        return radius;
    }
}
