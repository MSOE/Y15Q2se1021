package blocks.tiles;

import blocks.Position;

public class DLTurn extends Tile {

    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    public DLTurn(Position position) {
        super(position, 'L');
    }

    @Override
    public Move processMove(Move move) {
        Move nextMove = new Move(Move.State.STOP, move.position);
        if(move.state==Move.State.DOWN) {
            playerVisits();
            nextMove = new Move(Move.State.RIGHT, position);
        } else if(move.state==Move.State.LEFT) {
            playerVisits();
            nextMove = new Move(Move.State.UP, position);
        }
        return nextMove;
    }
}
