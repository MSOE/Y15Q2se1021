package blocks.tiles;

import blocks.Position;

public class FinishTile extends Tile {
    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    public FinishTile(Position position) {
        super(position, 'X');
    }

    @Override
    public Move processMove(Move move) {
        return new Move(Move.State.WIN, position);
    }
}
