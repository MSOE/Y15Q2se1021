package blocks.tiles;

import blocks.Position;

/**
 * Represents a left-right tunnel tile.  A player entering this
 * tile from the left or right will pass through the tile
 * maintaining their current direction.  A player approaching from
 * above or below the tile will be stopped prior to visiting the
 * tile.
 * @author taylor
 * @version 20150210.1
 */
public class LRTunnel extends Tile {

    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    public LRTunnel(Position position) {
        super(position, '=');
    }

    @Override
    public Move processMove(Move move) {
        Move nextMove = new Move(Move.State.STOP, move.position);
        if(move.state==Move.State.LEFT || move.state==Move.State.RIGHT) {
            playerVisits();
            nextMove = new Move(move.state, position);
        }
        return nextMove;
    }
}
