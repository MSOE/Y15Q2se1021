package blocks.tiles;

import blocks.Position;

public class Move {
    public enum State {UP, DOWN, LEFT, RIGHT, STOP, WIN, LOSE}

    public final State state;
    public final Position position;

    public Move(State state, Position position) {
        this.state = state;
        this.position = position;
    }
}
