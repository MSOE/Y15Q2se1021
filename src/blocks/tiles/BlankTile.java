package blocks.tiles;

import blocks.Position;

/**
 * Represents a blank tile.  A player entering this tile
 * from any direction will pass through the tile maintaining
 * their current direction.
 * @author taylor
 * @version 20150210.1
 */
public class BlankTile extends Tile {
    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    public BlankTile(Position position) {
        super(position, ' ');
    }
    public Move processMove(Move move) {
        playerVisits();
        return new Move(move.state, position);
    }
}
