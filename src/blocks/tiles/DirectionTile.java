package blocks.tiles;

import blocks.Position;

public class DirectionTile extends Tile {
    private Move.State direction;

    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    DirectionTile(Position position, char type) {
        super(position, type);
        switch (type) {
            case '^':
                direction = Move.State.UP;
                break;
            case 'v':
                direction = Move.State.DOWN;
                break;
            case '<':
                direction = Move.State.LEFT;
                break;
            case '>':
                direction = Move.State.RIGHT;
                break;
            default:
                throw new IllegalArgumentException("Direction tile must be one of the following types: ^, v, <, or >");
        }
    }
    @Override
    public Move processMove(Move move) {
        Move nextMove = new Move(Move.State.STOP, move.position);
        if(direction==move.state) {
            nextMove = new Move(move.state, position);
        }
        return nextMove;
    }
}
