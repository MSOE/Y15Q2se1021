package blocks.tiles;

import blocks.Position;

/**
 * A factory class whose sole purpose is to provide a method
 * that creates the appropriate Tile object based on the type.
 * @author taylor
 * @version 20150210.1
 */
public class TileFactory {
    /**
     * Instantiates the appropriate Tile object based on the
     * type passed to the method.
     * @param type Character describing the type of tile to be created
     * @param position The position of the tile on the GameBoard
     * @return A Tile matching the type
     */
    public static Tile createTile(char type, Position position) {
        Tile tile;
        switch(type) {
            case 'L':
                tile = new DLTurn(position);
                break;
            case 'J':
                tile = new DRTurn(position);
                break;
            case 'r':
                tile = new ULTurn(position);
                break;
            case '7':
                tile = new URTurn(position);
                break;
            case '=':
                tile = new LRTunnel(position);
                break;
            case 'H':
                tile = new UDTunnel(position);
                break;
            case '*':
                tile = new BombTile(position);
                break;
            case '^':
            case 'v':
            case '<':
            case '>':
                tile = new DirectionTile(position, type);
                break;
            case 'X':
                tile = new FinishTile(position);
                break;
            case '@':
            default:
                tile = new BlankTile(position);
                break;
        }
        return tile;
    }
}
