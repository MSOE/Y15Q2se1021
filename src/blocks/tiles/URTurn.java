package blocks.tiles;

import blocks.Position;

public class URTurn extends Tile {

    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    public URTurn(Position position) {
        super(position, '7');
    }

    @Override
    public Move processMove(Move move) {
        Move nextMove = new Move(Move.State.STOP, move.position);
        if(move.state==Move.State.UP) {
            playerVisits();
            nextMove = new Move(Move.State.LEFT, position);
        } else if(move.state==Move.State.RIGHT) {
            playerVisits();
            nextMove = new Move(Move.State.DOWN, position);
        }
        return nextMove;
    }
}
