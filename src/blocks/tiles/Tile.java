package blocks.tiles;

import blocks.Position;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Abstract tile class used as the foundation for a family
 * of tile classes representing positions on the GameBoard.
 * @author taylor
 * @version 20150210.1
 */
public abstract class Tile extends JLabel {
    /**
     * The position of the tile on the game board.
     */
    protected final Position position;

    /**
     * The character representing the appearance of the
     * tile.
     */
    private final char costume;

    /**
     * The character used to represent the appearance of
     * the player when the player is visiting the tile.
     */
    private static final char PLAYER_COSTUME = '@';

    /**
     * Used to create a border around the tile.
     */
    private static final Border border = BorderFactory.createLineBorder(Color.gray);

    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    protected Tile(Position position, char costume) {
        super("" + costume, SwingConstants.CENTER);
        this.position = position;
        this.costume = costume;
        setBorder(border);
    }

    /**
     * Processes one step of the player on the game board.
     * The method is passed the current move and determines
     * what the next move is based on the type of tile.
     * <p></p>
     * Each subclass must provide an implementation that is
     * appropriate for the type of tile the subclass represents.
     * @param move The current move
     * @return The next move based on visiting this tile
     */
    public abstract Move processMove(Move move);

    /**
     * Adjusts the tile's appearance based on the player
     * visiting the tile.
     */
    public void playerVisits() {
        setText("" + PLAYER_COSTUME);
    }

    /**
     * Restores the tile's appearance based on the player
     * leaving the current tile.
     */
    public void playerExits() {
        setText("" + costume);
    }

    public char getCostume() {
        return costume;
    }

    /**
     * Returns the position of the tile on the game board.
     * @return the position of the tile
     */
    public Position getPosition() {
        return position;
    }
}
