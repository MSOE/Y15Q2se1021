package blocks.tiles;

import blocks.Position;

public class UDTunnel extends Tile {

    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    public UDTunnel(Position position) {
        super(position, 'H');
    }

    @Override
    public Move processMove(Move move) {
        Move nextMove = new Move(Move.State.STOP, move.position);
        if(move.state==Move.State.UP || move.state==Move.State.DOWN) {
            playerVisits();
            nextMove = new Move(move.state, position);
        }
        return nextMove;
    }
}
