package blocks.tiles;

import blocks.Position;

public class ULTurn extends Tile {

    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    public ULTurn(Position position) {
        super(position, 'r');
    }

    @Override
    public Move processMove(Move move) {
        Move nextMove = new Move(Move.State.STOP, move.position);
        if(move.state==Move.State.UP) {
            playerVisits();
            nextMove = new Move(Move.State.RIGHT, position);
        } else if(move.state==Move.State.LEFT) {
            playerVisits();
            nextMove = new Move(Move.State.DOWN, position);
        }
        return nextMove;
    }
}
