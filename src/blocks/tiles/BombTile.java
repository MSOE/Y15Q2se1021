package blocks.tiles;

import blocks.Position;

public class BombTile extends Tile {

    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    public BombTile(Position position) {
        super(position, '*');
    }

    @Override
    public Move processMove(Move move) {
        return new Move(Move.State.LOSE, position);
    }
}
