package blocks.tiles;

import blocks.Position;

public class DRTurn extends Tile {

    /**
     * Constructor that accepts the position of the tile
     * on the GameBoard.
     * @param position Position of tile on GameBoard
     */
    public DRTurn(Position position) {
        super(position, 'J');
    }

    @Override
    public Move processMove(Move move) {
        Move nextMove = new Move(Move.State.STOP, move.position);
        if(move.state==Move.State.DOWN) {
            playerVisits();
            nextMove = new Move(Move.State.LEFT, position);
        } else if(move.state==Move.State.RIGHT) {
            playerVisits();
            nextMove = new Move(Move.State.UP, position);
        }
        return nextMove;
    }
}
