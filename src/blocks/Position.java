package blocks;

import java.io.Serializable;

public class Position implements Serializable {
    public final int col;
    public final int row;

    public Position(int row, int col) {
        this.col = col;
        this.row = row;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equal = obj==this;
        if(!equal && obj instanceof Position) {
            Position pos = (Position)obj;
            equal = pos.col==col && pos.row==row;
        }
        return equal;
    }
}
