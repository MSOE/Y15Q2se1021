package blocks;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Game extends JFrame {
    public static final int WIDTH = 436;
    public static final int HEIGHT = 129;
    private int level = 1;
    private final JPanel panel;
    private static final String BASE_FILENAME = "assets/levels/level";

    public static void main(String[] args) {
        new Game();
    }

    public Game() {
        setTitle("Block Slider");
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        GameBoard gameBoard = getBoard();
        panel = new JPanel();
        panel.setLayout(new BorderLayout());
        if(gameBoard!=null) {
            gameBoard.addActionListener(e -> handleAction(e));
            panel.add(gameBoard, BorderLayout.CENTER);
        }
        add(panel);
        setVisible(true);
    }

    private void handleAction(ActionEvent e) {
        if (e.getActionCommand().equals("Won")) {
            levelCleared();
        } else {
            handleLoss();
        }
    }

    private void nextLevel() {
        if(levelExists()) {
            GameBoard gameBoard = getBoard();
            panel.remove(((BorderLayout) panel.getLayout())
                    .getLayoutComponent(BorderLayout.CENTER));
            // I'm using a method reference below which is similar
            // the lambda expression used in the constructor above.
            gameBoard.addActionListener(this::handleAction);
            panel.add(gameBoard, BorderLayout.CENTER);
            pack();
            repaint();
        }
    }

    private void handleLoss() {
        int answer = JOptionPane.showConfirmDialog(this, "You lost.  Would you like to play again?");
        if(answer==JOptionPane.YES_OPTION) {
            level = 1;
            nextLevel();
        } else {
            System.exit(0);
        }
    }

    private void levelCleared() {
        if(levelExists()) {
            JOptionPane.showMessageDialog(this, "Congratulations, you cleared the level.");
            nextLevel();
        } else {
            int answer = JOptionPane.showConfirmDialog(this, "You win.  Would you like to play again?");
            if(answer==JOptionPane.YES_OPTION) {
                level = 1;
                nextLevel();
            } else {
                System.exit(0);
            }
        }
    }

    private GameBoard getBoard() {
        GameBoard gameBoard = null;
        try {
            gameBoard = new GameBoard(getFilename(), this);
            ++level;
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(this, "Could not read level", "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
        return gameBoard;
    }

    private boolean levelExists() {
        return Files.isReadable(Paths.get(getFilename()));
    }

    private String getFilename() {
        return BASE_FILENAME + level + ".txt";
    }
}
