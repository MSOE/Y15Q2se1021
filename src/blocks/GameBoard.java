package blocks;

import blocks.tiles.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.Scanner;

public class GameBoard extends JPanel implements Serializable {
    private Tile[][] gameBoard;
    private Position playerPosition;
    private ActionListener listener;
    private Component frame;
    private KeyHandler handler = new KeyHandler();

    public GameBoard(String filename, Component frame) throws FileNotFoundException {
        this.frame = frame;
        loadBoard(filename);
        createGUI();
        try {
            writeBoardBinary("assets/levels/level1.dat");
            writeBoardObject("assets/levels/level1.obj");
        } catch (IOException e) {
            System.err.println("Could not write the file. :(");
        }
        frame.addKeyListener(handler);
    }

    private void loadBoard(String filename) throws FileNotFoundException {
        int height = 0;
        int width;
        String characters = "";
        try (Scanner scan = new Scanner(new FileInputStream(filename))) {
            while(scan.hasNextLine()) {
                characters += scan.nextLine();
                height++;
            }
            width = characters.length()/height;
            gameBoard = new Tile[height][width];
            for(int row=0; row<height; row++) {
                for(int col=0; col<width; col++) {
                    char type = characters.charAt(row*width + col);
                    Position position = new Position(row, col);
                    gameBoard[row][col] = TileFactory.createTile(type, position);
                    if(type=='@') {
                        playerPosition = position;
                        gameBoard[row][col].playerVisits();
                    }
                }
            }
        }
    }

    public void addActionListener(ActionListener listener) {
        this.listener = listener;
    }

    public int numRows() {
        return gameBoard.length;
    }

    public int numCols() {
        return gameBoard[0].length;
    }

    private void createGUI() {
        setLayout(new GridLayout(numRows(), numCols()));
        for(int row=0; row<numRows(); ++row) {
            for(int col=0; col<numCols(); ++col) {
                add(gameBoard[row][col]);
            }
        }
    }

    public void writeBoardBinary(String filename) throws IOException {
        try (FileOutputStream fout = new FileOutputStream(filename);
             DataOutputStream dout = new DataOutputStream(fout)) {
            // write width/height
            dout.writeInt(numCols());
            dout.writeInt(numRows());
            // write player's position (row, col)
            dout.writeInt(playerPosition.row);
            dout.writeInt(playerPosition.col);
            for(int row=0; row<numRows(); ++row) {
                for(int col=0; col<numCols(); ++col) {
                    dout.writeChar(gameBoard[row][col].getCostume());
                }
            }
        }
    }

    public void writeBoardObject(String filename) throws IOException {
        try (FileOutputStream fout = new FileOutputStream(filename);
        ObjectOutputStream oout = new ObjectOutputStream(fout)) {
            oout.writeObject(this);
        }
    }

    private class KeyHandler implements KeyListener, Serializable {
        private static final int TIMER_DELAY = 100;
        private Move.State state;
        private Timer timer;
        private boolean isMoving = false;

        public void keyPressed(KeyEvent e) {
            int keyCode = e.getKeyCode();
            // Don't handle if player is currently moving or user
            // didn't hit one of the arrow keys.
            if(isMoving || !(keyCode==KeyEvent.VK_UP || keyCode==KeyEvent.VK_DOWN ||
                             keyCode==KeyEvent.VK_LEFT || keyCode==KeyEvent.VK_RIGHT)) {
                return;
            }
            switch (keyCode) {
                case KeyEvent.VK_UP:
                    state = Move.State.UP;
                    break;
                case KeyEvent.VK_DOWN:
                    state = Move.State.DOWN;
                    break;
                case KeyEvent.VK_LEFT:
                    state = Move.State.LEFT;
                    break;
                case KeyEvent.VK_RIGHT:
                    state = Move.State.RIGHT;
                    break;
            }
            timer = new Timer(TIMER_DELAY, event -> {
                Move currentMove = new Move(state, playerPosition);
                try {
                    Tile nextTile = findNextTile(state, playerPosition);
                    Move nextMove = nextTile.processMove(currentMove);
                    if(!nextMove.position.equals(currentMove.position)) {
                        gameBoard[playerPosition.row][playerPosition.col].playerExits();
                        nextTile.playerVisits();
                        state = nextMove.state;
                        playerPosition = nextTile.getPosition();
                    } else if(nextMove.state==Move.State.WIN) {
                        timer.stop();
                        if(listener!=null) {
                            listener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Won"));
                            frame.removeKeyListener(handler);
                        }
                    } else if(nextMove.state==Move.State.LOSE) {
                        timer.stop();
                        if(listener!=null) {
                            listener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Lost"));
                            frame.removeKeyListener(handler);
                        }
                    } else if(nextMove.state==Move.State.STOP) {
                        isMoving = false;
                        timer.stop();
                    }
                } catch(ArrayIndexOutOfBoundsException ex) {
                    gameBoard[playerPosition.row][playerPosition.col].playerExits();
                    timer.stop();
                    if(listener!=null) {
                        listener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "Lost"));
                        frame.removeKeyListener(handler);
                    }
                }
            });
            isMoving = true;
            timer.start();
        }

        private Tile findNextTile(Move.State state, Position position) {
            int row = position.row;
            int col = position.col;
            if(state==Move.State.UP) {
                --row;
            } else if(state==Move.State.DOWN) {
                ++row;
            } else if(state==Move.State.LEFT) {
                --col;
            } else if(state==Move.State.RIGHT) {
                ++col;
            }
            return gameBoard[row][col];
        }

        @Override
        public void keyTyped(KeyEvent e) {
            // nothing
        }

        @Override
        public void keyReleased(KeyEvent e) {
            // nothing
        }
    }

}
